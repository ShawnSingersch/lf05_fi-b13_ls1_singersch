﻿import java.util.Scanner; 
//Shawn Singersch | FiB-13 | 27.01.2022

class Fahrkartenautomat
{ 
	public static void main(String[] args) {
 	   
		Scanner tastatur = new Scanner(System.in);

		boolean isValid = true;
		double zuZahlenderBetrag;
		double rückgabebetrag;
		double eingezahlterGesamtbetrag;
		double betragtickets;
		int entscheider;
		entscheider = 1;
		char entscheider_leser;
 
		while (isValid == true) {
		zuZahlenderBetrag = fahrkartenbestellungErfassen();
		eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
		System.out.print("Möchten Sie noch weitere Tickets bestellen?\n");
		System.out.print("Schreiben Sie 'j' um weitere Tickets zu bestellen.\n");
		System.out.print("Schreiben Sie 'n' um das Program zu schließen\n");
		System.out.print("--> ");
		entscheider_leser = tastatur.next().charAt(0);
		if (entscheider_leser == 'n') {
			
			isValid = false;
			}
			
		}
}
	

       public static double fahrkartenbestellungErfassen() {
    	   Scanner tastatur = new Scanner(System.in);
    	   String[] fahrkartenBezeichnung = new String[10];
    	   double[] preis = new double [10];
    	   
    	   fahrkartenBezeichnung[0] = "1 Einzelfahrschein Berlin AB";
    	   fahrkartenBezeichnung[1] = "2 Einzelfahrschein Berlin BC";
    	   fahrkartenBezeichnung[2] = "3 Einzelfahrschein Berlin ABC";
    	   fahrkartenBezeichnung[3] = "4 Kurzstrecke";
    	   fahrkartenBezeichnung[4] = "5 Tageskarte Berlin AB";
    	   fahrkartenBezeichnung[5] = "6 Tageskarte Berlin BC";
    	   fahrkartenBezeichnung[6] = "7 Tageskarte Berlin ABC";
    	   fahrkartenBezeichnung[7] = "8 Kleingruppen-Tageskarte Berlin AB";
    	   fahrkartenBezeichnung[8] = "9 Kleingruppen-Tageskarte Berlin BC";
    	   fahrkartenBezeichnung[9] = "10 Kleingruppen-Tageskarte Berlin ABC";
 
    	   preis[0] = 2.90;
    	   preis[1] = 3.30;
    	   preis[2] = 3.60;
    	   preis[3] = 1.90;
    	   preis[4] = 8.60;
    	   preis[5] = 9.00;
    	   preis[6] = 9.60;
    	   preis[7] = 23.50;
    	   preis[8] = 24.30;
    	   preis[9] = 24.90;
    	   
    	   
    	   System.out.println("Wählen sie ihr ticket");
    	   for (int i = 0; i < fahrkartenBezeichnung.length;i++) {
    		   System.out.print("\n");
    		   System.out.println(fahrkartenBezeichnung[i]);
    		   System.out.printf("%.2f",preis[i]);
    		   System.out.print("€");
    		   System.out.print("\n");
    	   }
    	   
    	   System.out.print("\nAuswahl Ticket: ");
    	   int ticket = tastatur.nextInt();
    	   
    	   if (ticket < 1 || ticket > fahrkartenBezeichnung.length) {
    		   System.out.println("Fehlerhafte Eingabe!");
    	   }
    	   
    	   System.out.print("\n");
    	   System.out.printf("Anzahl der Tickets: ");
    	   int anzahl = tastatur.nextInt();
    	   
    	   
    	   double betrag = anzahl * preis[ticket - 1];
    	   
    	   System.out.printf("\nZu zahlen: ", "%.2f", + betrag + "€");
    	   return betrag;
    		   
    	   
    	  
//    	   int ticketauswahl;
//    	   int anzahltickets;
//    	   System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus ('1', '2' oder '3' eintippen: \n");
//    		   System.out.print("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n");
//    		   System.out.print("Tageskarte Regeltarif AB [8,60 EUR] (2)\n");
//    		   System.out.print("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n");
//    	   ticketauswahl = tastatur.nextInt();
//    	   double tarifbetrag = 0.0;
//    	   if (ticketauswahl == 1) {
//    		   
//    		   tarifbetrag = 2.90;
//    		   
//    	   }
//    	   
//    	   else if (ticketauswahl == 2)
//    	   {
//    		   tarifbetrag = 8.60;
//    		   
//    	   }
//    	   
//    	   else if (ticketauswahl == 3)
//    	   {
//    		   tarifbetrag = 23.50;
//    	   }
//    	   
//		   double betragtickets;
//    	   System.out.printf("\nAnzahl der Tickets eingeben: ");
//    	   anzahltickets = tastatur.nextInt();
//    	   
//    	   
//		   betragtickets = tarifbetrag * anzahltickets; //Leider Stoße ich hier an einem Problem: Ich möchte, wenn die Ticketauswahl z.b '1' ist, dass die Variable tarifbetrag den Wert 2,80 zugewiesen werden soll Anscheinend wird die Wertzuweisung außerhalb der If schleife nicht angewedent. Ich bitte um hilfe. 
//    	   System.out.printf("\nZu zahlender Betrag (EURO): ");
//    	   System.out.printf("%.2f",betragtickets);
//    	   System.out.print(" Euro");
//     	   return betragtickets;
         }
       
    // Geldeinwurf 
       public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	   Scanner tastatur = new Scanner(System.in);
           double eingezahlterGesamtbetrag;
           double eingeworfeneMünze;
    	   eingezahlterGesamtbetrag = 0.0;
           while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
           {   
        	   System.out.printf("Noch zu zahlen: ");
        	   System.out.printf("%.2f",zuZahlenderBetrag - eingezahlterGesamtbetrag );
        	   System.out.printf("€\n");
        	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
        	   eingeworfeneMünze = tastatur.nextDouble();
               eingezahlterGesamtbetrag += eingeworfeneMünze;
           }
    	   //zuZahlenderBetrag = tastatur.nextDouble();
    	   return eingezahlterGesamtbetrag;
    	   
}
       

       // Fahrscheinausgabe
       public static void fahrkartenAusgeben() {
    	   
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
       
      }
       
       
       // Rückgeldberechnung und -Ausgabe
       public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	
       double rückgabebetrag;
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.print("Der Rückgabebetrag in Höhe von ");
    	   System.out.printf("%.2f", rückgabebetrag);
    	   System.out.print(" EURO ");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

    	   
           if(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2,00 EURO");
	          rückgabebetrag -= 2.0;
           }
           if(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1,00 EURO");
	          rückgabebetrag -= 1.0;
           }
           if(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("0,50 EURO");
	          rückgabebetrag -= 0.5;
           }
           if(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("0,40 EURO");
 	          rückgabebetrag -= 0.2;
           }
           if(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("0,10 EURO");
	          rückgabebetrag -= 0.1;
           }
           if(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("0,05 EURO");
 	          rückgabebetrag -= 0.05;
           }
       }
   

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       
       
	}
}
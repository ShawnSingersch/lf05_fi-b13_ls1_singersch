import java.util.ArrayList;

public class Raumschiff {

	//Variablen
	
	private int photonentorpedoAnzahl;
	private int energieversorgungprozent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	//Konstruktor
	
	public Raumschiff() {}
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungprozent, 
			int zustandSchildeInProzent, int zustandHuelleInProzent, int zustandLebenserhaltungssystemeInProzent,
			int anzahlDroiden, String schiffsname) {
	
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungprozent = energieversorgungprozent;
		this.schildeInProzent = zustandSchildeInProzent;
		this.huelleInProzent = zustandHuelleInProzent;
		this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
		this.androidenAnzahl = anzahlDroiden;
		this.schiffsname = schiffsname;
	}
	
	
	 // setter und getter
	  public void setEnergieversorgungprozent(int energieversorgungprozent) { this.energieversorgungprozent = energieversorgungprozent; }

	  public int getEnergieversorgungprozent() { return this.energieversorgungprozent; }

	  public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) { this.photonentorpedoAnzahl = photonentorpedoAnzahl; }

	  public int getPhotonentorpedoAnzahl() { return this.energieversorgungprozent; }

	  public void setSchildeInProzent(int schildeInProzent) { this.schildeInProzent = schildeInProzent; }

	  public int getSchildeInProzent() { return this.schildeInProzent; }

	  public void setHuelleInProzent(int huelleInProzent) { this.huelleInProzent = huelleInProzent; }

	  public int getHuelleInProzent() { return this.huelleInProzent; }

	  public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) { this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent; }

	  public int getlebenserhaltungssystemeInProzent() { return this.lebenserhaltungssystemeInProzent; }

	  public void setAndroidenAnzahl(int androidenAnzahl) { this.androidenAnzahl = androidenAnzahl; }

	  public int getandroidenAnzahl() { return this.androidenAnzahl; }

	  public void setSchiffsname(String schiffsname) { this.schiffsname = schiffsname; }

	  public String getSchiffsname() { return this.schiffsname; }

	  public void addLadung(Ladung neueLadung) {}
	  
	
}

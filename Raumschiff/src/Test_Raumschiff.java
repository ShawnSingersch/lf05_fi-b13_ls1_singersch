//Shawn Singersch | FiB-13 | 02.06.2022
public class Test_Raumschiff {

	public static void main(String[] args) {
		// Ladung erstellen
		
		Ladung L1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung L2 = new Ladung("Borg-Schrott", 5);
		Ladung L3 = new Ladung ("Rote Materie", 2);
		Ladung L4 = new Ladung ("Forschungssonde", 35);
		
		//Raumschiffe erzeugen
		
		Raumschiff klingonen = new Raumschiff(1,100,100,100,100,2,"IKS Hegh�ta");
		Raumschiff romulaner = new Raumschiff(2,100,100,100,100,2,"IRW Khazara");
		Raumschiff vulkanier = new Raumschiff(0,80,80,50,100,5,"Ni'Var'");
		
		//Ladung 2 erstellen
		
		Ladung L5 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung L6 = new Ladung("Plasma-Waffe",50);
		Ladung L7 = new Ladung("Photonentorpedo",3);
		
		//Ladungszuweisung
		
			//Raumschiff Klingonen
			klingonen.addLadung(L1);
			klingonen.addLadung(L5);
			
			//Raumschiff Romulaner
			romulaner.addLadung(L2);
			romulaner.addLadung(L3);
			romulaner.addLadung(L6);
			
			//Raumschiff Vulkanier
			vulkanier.addLadung(L4);
			vulkanier.addLadung(L7);
	}

}

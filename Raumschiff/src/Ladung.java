// Shawn Singersch | FiB-13 | 12.05.2022
public class Ladung {
	//Atribute
	private String bezeichnung;
	private int menge;
	
	//Konstruktor
	public Ladung() {}
	
	public Ladung (String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	//setter
	public void setBezeichnung(String name) {this.bezeichnung = name; }
	public void setMenge(int menge) {this.menge = menge; }
	
	//getter
	
	public String getBezeichnung() {return this.bezeichnung; }
	public int getMenge() {return this.menge; }
	
	@Override
	public String toString() {
		return "Der Name der Ladung lautet: " + bezeichnung +"\n"+ 
				"Die Menge der Ladung betr�gt: " + menge + " St�ck";
	}
	
	
}

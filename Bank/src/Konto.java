
public class Konto {
  // Attribute
  private String iban;
  private int kontonr;
  private double kontostand;
  
  // Konstruktoren
  public Konto () {}
  
  public Konto (String iban, int kontonr, double kontostand) {
    this.iban = iban;
    this.kontonr = kontonr;
    this.kontostand = kontostand;
  }
  
  // setter
  public void setIban(String iban) { this.iban = iban; }
  
  public void setKontonr(int kontonr) { this.kontonr = kontonr; }
  
  public void setKontostand(double kontostand) { this.kontostand = kontostand; }
  
  // getter
  public String getIban() { return this.iban; }
  
  public int getKontonr() { return this.kontonr; }
  
  public double getKontostand() { return this.kontostand; }
  
  // Methoden
  public void geldAbheben(double menge) {
    this.kontostand -= menge;
  }
  
  public void geldEinzahlen(double menge) {
    this.kontostand += menge;
  }
  
  public void geldUeberweisen(double menge, Konto ziel) {
    this.kontostand -= menge;
    ziel.geldEinzahlen(menge);
  }
}
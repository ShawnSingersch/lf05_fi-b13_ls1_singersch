import java.util.Scanner;
public class aufgabe6_millionenrechner {

	public static void main(String[] Args) {

		Scanner Bildschirm = new Scanner(System.in);
		int i = 1;
		char eingabe = 'j';
		double Kapital; 
		double zinsensatzh�he;

		while (eingabe == 'j') {

			System.out.print("Bitte geben Sie an, wie viel Geld Sie anlegen m�chten: ");
			Kapital = Bildschirm.nextDouble();

			System.out.print("Bitte geben Sie den Zinssatz an: ");
			zinsensatzh�he = Bildschirm.nextDouble();
			zinsensatzh�he = zinsensatzh�he / 100 + 1;

			while (Kapital < 1000000) {
				Kapital = Kapital * zinsensatzh�he;
				i++;
			}
			System.out.println("In " + i + " Jahren sind Sie Million�r und besitzen 1.000.000 Euro");
			
			System.out.println("M�chten Sie eine weitere Rechnung starten?\n"); 
			System.out.print("'j' f�r Ja\n");
			System.out.print("'n' f�r Nein\n");
			System.out.print("--> ");
			eingabe = Bildschirm.next().charAt(0);
		}
	}
}
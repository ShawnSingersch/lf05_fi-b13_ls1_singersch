import java.util.Scanner;

public class taschenrechner_aufgabe4 {

	public static void main(String[] args) {
		Scanner Eingabe = new Scanner(System.in);
		double erstezahl;
		double zweitezahl;
		char rechenoperation;
		
		
		System.out.print("Geben sie die erste Zahl ein: ");
		erstezahl = Eingabe.nextDouble();
		System.out.print("Welche Rechenoperation m�chten Sie durchf�hren?\n");
		System.out.print("\n'+' f�r Addition");
		System.out.print("\n'-' f�r Subtraktion");
		System.out.print("\n'*' f�r Multiplikation");
		System.out.print("\n'/' f�r Division");
		System.out.print("\nBitte hier eingeben: ");
		rechenoperation = Eingabe.next().charAt(0);
		System.out.print("Geben sie die zweite Zahl ein: ");
		zweitezahl = Eingabe.nextDouble();
		double ergebnis;
		
		//Addition
		if(rechenoperation == '+') {
			ergebnis = erstezahl + zweitezahl;
			System.out.print(erstezahl + " + " + zweitezahl + " = " + ergebnis);
		}
		
		//Subtraktion
		if(rechenoperation == '-') {
			ergebnis = erstezahl - zweitezahl;
			System.out.print(erstezahl + " - " + zweitezahl + " = " + ergebnis);
		}
		
		//Multiplikation
		if(rechenoperation == '*') {
			ergebnis = erstezahl * zweitezahl;
			System.out.print(erstezahl + " x " + zweitezahl + " = " + ergebnis);
		}
		
		//Division
		if(rechenoperation == '/') {
			ergebnis = erstezahl / zweitezahl;
			System.out.print(erstezahl + " / " + zweitezahl + " = " + ergebnis);
		}
		
		if(rechenoperation != '+' && rechenoperation != '-' && rechenoperation != '*' && rechenoperation != '/' ) {
			System.out.print("Fehler! Bitte starten Sie das Porgram neu");
		}	
	}

}

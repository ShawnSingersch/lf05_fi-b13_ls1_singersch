
public class aufgabe5_einmaleins {

	public static void main(String[] args) {
		
		int i = 0;
		int faktor1 = 1;
		int faktor2 = 1;
		int produkt;
		while (faktor2 < 11 ) {
			produkt = faktor1*faktor2;
			System.out.print("\n"+produkt);
			faktor1 += 1;
			if(faktor1 == 11) {
				System.out.print("\n");
				faktor1 = 1;
				faktor2 ++;
			}
			
		}// Ende der While schleife
	}

}

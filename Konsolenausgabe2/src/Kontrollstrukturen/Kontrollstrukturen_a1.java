package Kontrollstrukturen;

public class Kontrollstrukturen_a1{

	public static void main(String[] args) {
		
		int x = 7;
		int y = 9;
	
		if(x<y)
		{
			System.out.print("Ich schlafe jetzt weiter! Es ist erst 7:00 Uhr!");
		}
		
		else
		{
			System.out.print("Es ist 9:00 Uhr. Zeit zum aufstehen.");
		}
	}
}

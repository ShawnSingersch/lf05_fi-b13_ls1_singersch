import java.util.Scanner;

public class aufgabe6_matrix {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);

		System.out.print("Bitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
		int eingabe = tastatur.nextInt();
		int i = 0;
		int ersteStelle;
		int zweiteStelle;
		
		System.out.println("");
		
		while (i < 100) {
			
				ersteStelle = i / 10;
				zweiteStelle = i % 10;
			if (i % eingabe == 0)  {
				System.out.printf("%4s", "*");
			}
			
			else if ( ersteStelle == eingabe || zweiteStelle == eingabe) {
				System.out.printf("%4s", "*");
			}
			
			
			else if ( ersteStelle + zweiteStelle == eingabe) {
				System.out.printf("%4s", "*");
			}
			
			
			else {
				System.out.printf("%4d", i);
			}
			
			i++;
			
			if (i % 10 == 0 && i != 0) {
				System.out.println();
			}
		}
	}
}

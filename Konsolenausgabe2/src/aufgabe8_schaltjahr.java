import java.util.Scanner;

public class aufgabe8_schaltjahr {

	public static void main(String[] args) {
		
		Scanner Jahreseingabe = new Scanner(System.in);
		double usereingabe;
		
		System.out.print("Hallo geben Sie das Jahr ein, was sie �berpr�fen m�chten: ");
		usereingabe = Jahreseingabe.nextDouble();
		
		//Sonderfall
		if (usereingabe == 1582) {
			System.out.print("Das ist tats�chlich ein Schaltjahr");
		}
		
		if (usereingabe % 4 == 0) {
			System.out.printf("\nDas Jahr ist ein Schaltjahr!");
			if (usereingabe % 100 == 0 && usereingabe % 400 != 0) {
				System.out.printf("\nDas Jahr ist ein Schaltjahr!!");
			}
		}
		
		if(usereingabe != 1582 && usereingabe % 4 < 0) {
			System.out.print("\nDas Jahr ist leider kein Schaltjahr.");
		}
	}
}
